//
//  SideMenuModel.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import Foundation

struct SideMenuModel {
    var icon: UIImage
    var title: String
}
