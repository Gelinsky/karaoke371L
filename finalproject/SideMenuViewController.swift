//
//  SideMenuViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import UIKit

protocol SideMenuViewControllerDelegate {
    func selectedCell(_ row: Int)
}

class SideMenuViewController: UIViewController {
   
    @IBOutlet weak var sideMenuTableView: UITableView!
    var defaultHighlightedCell: Int = 0
    
    var delegate: SideMenuViewControllerDelegate?

    var menu: [SideMenuModel] = [
        SideMenuModel(icon: UIImage(systemName: "music.note")!, title: "Join/Create Room"),
        SideMenuModel(icon: UIImage(systemName: "person.fill")!, title: "Change Name"),
        SideMenuModel(icon: UIImage(systemName: "person.2.fill")!, title: "View Members"),
        SideMenuModel(icon: UIImage(systemName: "music.note.list")!, title: "Queue"),
        SideMenuModel(icon: UIImage(systemName: "magnifyingglass")!, title: "Add Song"),
        SideMenuModel(icon: UIImage(systemName: "music.mic")!, title: "Lyrics"),
        SideMenuModel(icon: UIImage(systemName: "slider.horizontal.3")!, title: "Settings")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // TableView
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
        self.sideMenuTableView.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        self.sideMenuTableView.separatorStyle = .none

        // Set Highlighted Cell
        DispatchQueue.main.async {
            let defaultRow = IndexPath(row: self.defaultHighlightedCell, section: 0)
            self.sideMenuTableView.selectRow(at: defaultRow, animated: false, scrollPosition: .none)
        }

        // Footer
//        self.footerLabel.textColor = UIColor.white
//        self.footerLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
//        self.footerLabel.text = "Developed by John Codeos"

        // Register TableView Cell
        self.sideMenuTableView.register(SideMenuCell.nib, forCellReuseIdentifier: SideMenuCell.identifier)

        // Update TableView with the data
        self.sideMenuTableView.reloadData()
    }
}

// MARK: - UITableViewDelegate

extension SideMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

// MARK: - UITableViewDataSource

extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier, for: indexPath) as? SideMenuCell else { fatalError("xib doesn't exist") }

        cell.iconImageView.image = self.menu[indexPath.row].icon
        cell.titleLabel.text = self.menu[indexPath.row].title

        // Highlighted color
        let myCustomSelectionColorView = UIView()
        myCustomSelectionColorView.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        cell.selectedBackgroundView = myCustomSelectionColorView
        
//        self.delegate?.selectedCell(indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // ...
        self.delegate?.selectedCell(indexPath.row)
//         Remove highlighted color when you press the 'Profile' and 'Like us on facebook' cell
//        if indexPath.row == 4 || indexPath.row == 6 {
            tableView.deselectRow(at: indexPath, animated: true)
//        }
    }
}
