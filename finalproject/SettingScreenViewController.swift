//
//  SettingScreenViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import UIKit
import CoreData
import FirebaseFirestore

var lightMode:UIColor! = UIColor(red: 0/255, green: 223/255, blue: 239/255, alpha: 1.0)
var backgroundMode: UIColor! = lightMode

class SettingScreenViewController: UIViewController {

    @IBOutlet weak var darkModeSwitch: UISegmentedControl!
    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        if backgroundMode != lightMode {
            darkModeSwitch.selectedSegmentIndex = 1
        }
        view.backgroundColor = backgroundMode

        // Do any additional setup after loading the view.
    }
    
    @IBAction func leaveRoomPushed(_ sender: Any) {
        print("this is the global room code \(globalRoomCode)")
        print("this is the current name!!!!: \(name)")
        let db = Firestore.firestore()
        let docRef = db.collection("rooms").document(globalRoomCode)
        docRef.getDocument {(document, error) in
            if let document = document, document.exists {
                var memberArray = document["memberNames"] as? Array ?? [""]
                let queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
                let queuedSongURLsArray = document["queuedSongUrls"] as? Array ?? []
                print(memberArray)
                print("here is the name \(name)")
                let index = memberArray.firstIndex(of: name!)
                print("this is the index: \(index)")
                if (index != nil) {
                    memberArray.remove(at: index!)
                    print("remove name successful")
                    let docData: [String: Any] = [
                        "memberNames": memberArray,
                        "queuedSongNames": queuedSongNamesArray,
                        "queuedSongUrls": queuedSongURLsArray
                    ]
                    db.collection("rooms").document(globalRoomCode).setData(docData) { err in
                        if let err = err {
                            print("error writing doc")
                        }
                        else {
                            print("doc written successfully")
                            globalRoomCode = ""
                            name = ""
                            self.performSegue(withIdentifier: "LeaveRoomSegueIdentifier", sender: nil)
                        }
                    }
                }
                else {
                    print("error in finding name")
                }
            }
        }
    }
    
    @IBAction func switchDarkMode(_ sender: Any) {
        
        if darkModeSwitch.selectedSegmentIndex == 1 {
            backgroundCore.setValue(0, forKey: "background")
            print("changed the background in core to dark mode")
            backgroundMode = UIColor.black
        }
        else {
            backgroundCore.setValue(1, forKey: "background")
            print("changed the background in core to light mode")
            backgroundMode = lightMode
        }
        // save to core data
        do {
           try context.save()
        } catch {
            // If an error occurs
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        view.backgroundColor = backgroundMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    
    // don't want to segue immediately, only on successful sign in/up
    override func shouldPerformSegue(withIdentifier identifier:String, sender: Any?) -> Bool {
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
