//
//  ViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/6/21.
//

import UIKit
import Firebase
import CoreData
import FirebaseFirestore

var globalRoomCode:String! = ""
var name: String! = ""

var backgroundCore: NSManagedObject! = nil
var context: NSManagedObjectContext! = nil

class ViewController: UIViewController, SPTSessionManagerDelegate, SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate, SPTAppRemotePlayerAPI {
    var delegate: SPTAppRemotePlayerStateDelegate?
    var container: NSPersistentContainer!


    @IBOutlet weak var roomCodeTextField: UITextField!
    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    var roomCode: String! = nil
    
    let SpotifyClientID = "16097941e606440bb95e06469291dc52"
    let SpotifyRedirectURL = URL(string: "KaraOK://spotify-login-callback")!

    lazy var configuration: SPTConfiguration = {
        let configuration = SPTConfiguration(clientID: SpotifyClientID, redirectURL: SpotifyRedirectURL)
        configuration.playURI = ""
        configuration.tokenSwapURL = URL(string: "https://karaok371l.herokuapp.com/api/token")
        configuration.tokenRefreshURL = URL(string: "https://karaok371l.herokuapp.com/api/refresh_token")
        return configuration
    }()
    
    lazy var sessionManager: SPTSessionManager = {
        let manager = SPTSessionManager(configuration: configuration, delegate: self)
        return manager
    }()
    
    lazy var appRemote: SPTAppRemote = {
            let appRemote = SPTAppRemote(configuration: configuration, logLevel: .debug)
            appRemote.delegate = self
            return appRemote
        }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"BackgroundEntity")
        var fetchedResults:[NSManagedObject]? = nil

        do {
            try fetchedResults = context.fetch(request) as? [NSManagedObject]
        } catch {
            // If an error occurs
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        
        print(fetchedResults)
        if (fetchedResults != nil && fetchedResults?.count ?? 0 > 0) {
            print("not nil")
            for item in fetchedResults! {
                backgroundCore = item
                let background = item.value(forKey:"background")!
                print("current value of background \(background)!!!!")
                if (background as! Int == 0) {
                        // dark mode
                        print("in vc, dark mode")
                        backgroundMode = UIColor.black
                    }
                    else {
                        print("in vc, light mode")
                        backgroundMode = lightMode
                    }
            }
        }
        else {
            print("first storing background")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext

            let newBackground = NSEntityDescription.insertNewObject(
                forEntityName: "BackgroundEntity", into: context)

            // set the values
            newBackground.setValue(false, forKey: "background")
          
            // save to core data
            do {
               try context.save()
            } catch {
                // If an error occurs
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
            backgroundCore = newBackground
        }
        view.backgroundColor = backgroundMode
    
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    
    // SPTSessionManagerDelegate
    func sessionManager(manager: SPTSessionManager, didFailWith error: Error) {
        presentAlertController(title: "Authorization Failed", message: error.localizedDescription, buttonTitle: "Oh no")
    }

    func sessionManager(manager: SPTSessionManager, didRenew session: SPTSession) {
        presentAlertController(title: "Session Renewed", message: session.description, buttonTitle: "Nice!")
    }

    func sessionManager(manager: SPTSessionManager, didInitiate session: SPTSession) {
        appRemote.connectionParameters.accessToken = session.accessToken
        globalStruct.accessToken = session.accessToken
        DispatchQueue.main.async {
            self.appRemote.connect()
        }
//        enqueueTrackUri("spotify:track:51EMSRpNm9Rg5rGViVCczv")
//        enqueueTrackUri("spotify:track:1ChulFMnwxoD74Me8eX2TU")
//        enqueueTrackUri("spotify:track:51EMSRpNm9Rg5rGViVCczv")
//        enqueueTrackUri("spotify:track:1ChulFMnwxoD74Me8eX2TU")
    }
    // code to enable tapping on the background to remove software keyboard
        
        func textFieldShouldReturn(textField:UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }

    // don't want to segue immediately, only on successful sign in/up
    override func shouldPerformSegue(withIdentifier identifier:String, sender: Any?) -> Bool {
        return false
    }
    
    @IBAction func pressedCreateRoom(_ sender: Any) {
        // prompt Spotify authentication
        
        let scope: SPTScope = [.appRemoteControl, .userModifyPlaybackState]
        
        if #available(iOS 11, *) {
            // Use this on iOS 11 and above to take advantage of SFAuthenticationSession
            sessionManager.initiateSession(with: scope, options: .clientOnly)
        } else {
            // Use this on iOS versions < 11 to use SFSafariViewController
            sessionManager.initiateSession(with: scope, options: .clientOnly, presenting: self)
        }
        // generate a four-digit room number
        // create a document with that room number
        // store that document
        // check to see if room already exists
        roomCode = ""
        while (roomCode == "") {
            roomCode = generateRoomCode()
            print("generated roomID: \(roomCode)")
            print("looping")
            
            let db = Firestore.firestore()
            let docRef = db.collection("rooms").document(String(roomCode))
            print("here")
            docRef.getDocument {(document, error) in
                if let document = document, document.exists {
                    print("inside if")
                    print("Document data: \(document.data())")
                    self.roomCode = String(self.roomCode)
                    print("stored room code: \(self.roomCode)")
                    self.roomCode = ""
                } else {
                    print("inside else")
                    print("Document does not exist")
                    let db = Firestore.firestore()
                    let docData: [String: Any] = [
                            "memberNames": [],
                            "queuedSongNames": [],
                            "queuedSongUrls": []
                    ]
                    print("room code: \(self.roomCode)")
                    db.collection("rooms").document(self.roomCode).setData(docData) { err in
                        if let err = err {
                            print("error writing doc")
                        }
                        else {
                            print("doc written successfully")
                            globalRoomCode = self.roomCode
                            print("global room code \(globalRoomCode)")
                            self.performSegue(withIdentifier: "CreatePressedSegueIdentifier", sender: nil)
                        }
                    }
                }
            }
            print("end of while")
        }
    }
        
    func generateRoomCode() -> String {
        var fourDigitNumber: String {
            var result = ""
            repeat {
                result = String(format: "%04d", arc4random_uniform(10000))
            } while Set<Character>(result).count < 4
            return result
        }
        return fourDigitNumber
    }
    
    @IBAction func joinRoomPressed(_ sender: Any) {
        // check to see if a document exists titled that
        // room number from room code text field
        // store that document
        // if does not exist, send error alert message
        
        // room code is not the right length
        if (roomCodeTextField.text?.count != 4) {
            let alert = UIAlertController(title: "Invalid room code entered", message: "Room code must be 4 numbers", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
            self.present(alert, animated: true)
        }
        let enteredRoomCode = String(roomCodeTextField.text!)
        print("entered room code: \(enteredRoomCode)")
        print("string version of room code: \(String(enteredRoomCode))")
        // room code is not 4 numbers
//        if (enteredRoomCode.count != 4) {
//            let alert = UIAlertController(title: "Invalid room code entered", message: "Room code must be 4 numbers", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
//            self.present(alert, animated: true)
//        }
        // check to see if the document exists
        let db = Firestore.firestore()
        let docRef = db.collection("rooms").document(String(enteredRoomCode))
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                print("Document data: \(document.data())")
                self.roomCode = String(enteredRoomCode)
                print("stored room code: \(self.roomCode)")
                globalRoomCode = self.roomCode
                self.performSegue(withIdentifier: "JoinPressedSegueIdentifier", sender: nil)
            } else {
                let alert = UIAlertController(title: "Invalid room code entered", message: "Room does not exist", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
                self.present(alert, animated: true)
                print("Document does not exist")
            }
        }
    }
    
    private func presentAlertController(title: String, message: String, buttonTitle: String) {
        DispatchQueue.main.async {
            let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
            controller.addAction(action)
            self.present(controller, animated: true)
        }
    }
    
   
    
    // SPTAppRemoteDelegate
    
    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        appRemote.playerAPI?.delegate = self
        appRemote.playerAPI?.subscribe(toPlayerState: { (success, error) in
            if let error = error {
                print("Error subscribing to player state:" + error.localizedDescription)
            }
        })
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        
    }
    
    // SPTAppRemotePlayerStateDelegate
    
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        print("HEY LOOK AT ME")
    }
    
    // SPTAppRemotePlayerAPI
    
    func play(_ entityIdentifier: String, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func play(_ trackUri: String, asRadio: Bool, callback: @escaping SPTAppRemoteCallback) {
        
    }
    
    func play(_ contentItem: SPTAppRemoteContentItem, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func play(_ contentItem: SPTAppRemoteContentItem, skipToTrackIndex index: Int, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func resume(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func pause(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func skip(toNext callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func skip(toPrevious callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func seek(toPosition position: Int, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func seekForward15Seconds(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func seekBackward15Seconds(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func setShuffle(_ shuffle: Bool, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func setRepeatMode(_ repeatMode: SPTAppRemotePlaybackOptionsRepeatMode, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func getPlayerState(_ callback: SPTAppRemoteCallback? = nil) {
        
        
    }
    
    func subscribe(toPlayerState callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func unsubscribe(toPlayerState callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func enqueueTrackUri(_ trackUri: String, callback: SPTAppRemoteCallback? = nil) {
    }
    
    func getAvailablePodcastPlaybackSpeeds(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func getCurrentPodcastPlaybackSpeed(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func setPodcastPlaybackSpeed(_ speed: SPTAppRemotePodcastPlaybackSpeed, callback: SPTAppRemoteCallback? = nil) {
        
    }
    
    func getCrossfadeState(_ callback: SPTAppRemoteCallback? = nil) {
        
    }
}

