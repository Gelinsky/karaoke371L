//
//  AddSongViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import UIKit
import Alamofire
import FirebaseFirestore
import SwiftyJSON

class AddSongViewController: UIViewController {

    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    @IBOutlet weak var addSongTextField: UITextField!
    lazy var VC = ViewController()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
        view.backgroundColor = backgroundMode
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    
    // code to enable tapping on the background to remove software keyboard
        
        func textFieldShouldReturn(textField:UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
   
    @IBAction func pressedAddToQueue(_ sender: Any) {
        
        
        
        var artistName: String = ""
        var songName: String = ""
        var songURI: String = ""
        
        var searchQuery: String = ""
        let words = addSongTextField.text!.components(separatedBy: " ")
        for word in words{
            searchQuery += word + "%20"
        }
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + globalStruct.accessToken
        ]
        
        AF.request("https://api.spotify.com/v1/search?q=" + searchQuery + "&type=track&limit=1", encoding:JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                let artistNameResult = json["tracks"]["items"][0]["artists"][0]["name"]
                if let artistNameString = artistNameResult.string {
                    artistName = artistNameString
                    print(artistNameString)
                } else {
                    print(artistNameResult.error!)
                }
                let songNameResult = json["tracks"]["items"][0]["name"]
                if let songNameString = songNameResult.string {
                    songName = songNameString
                    print(songNameString)
                } else {
                    print(songNameResult.error!)
                }
                let songURIResult = json["tracks"]["items"][0]["uri"]
                if let songURIString = songURIResult.string {
                    songURI = songURIString
                    print(songURIString)
                } else {
                    print(songURIResult.error!)
                }
                AF.request("https://api.spotify.com/v1/me/player/queue?uri=" + songURI, method: .post, headers: headers).responseData { response in
                    debugPrint(response)
                }
                let db = Firestore.firestore()
                let docRef = db.collection("rooms").document(String(globalRoomCode))
                docRef.getDocument {(document, error) in
                    if let document = document, document.exists {
                        let memberArray = document["memberNames"] as? Array ?? [""]
                        var queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
                        var queuedSongURLsArray = document["queuedSongUrls"] as? Array ?? []

                        
                        queuedSongNamesArray.append(artistName + " - " + songName)
                        queuedSongURLsArray.append(songURI)
                        let docData: [String: Any] = [
                            "memberNames": memberArray,
                            "queuedSongNames": queuedSongNamesArray,
                            "queuedSongUrls": queuedSongURLsArray
                        ]
                        if songName != ""{
                            docRef.setData(docData) { err in
                                if err != nil {
                                    print("error writing doc")
                                }
                                else {
                                    print("doc written successfully")
                                }
                            }
                        }
                    } else {
                        print("ERROR in AddSongVC: Room code not valid")
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

