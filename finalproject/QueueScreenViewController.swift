//
//  QueueScreenViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import UIKit
import Firebase
import FirebaseFirestore

class QueueScreenViewController: UIViewController {
    
    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    @IBOutlet weak var currentlyPlayingLabel: UILabel!
    @IBOutlet weak var playingListLabel: UILabel!
    var roomCode: String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
        view.backgroundColor = backgroundMode

//        roomCode = globalRoomCode
        let db = Firestore.firestore()
        let docRef = db.collection("rooms").document(globalRoomCode)
        docRef.getDocument {(document, error) in
            if let document = document, document.exists {
                let queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
                if queuedSongNamesArray.isEmpty {
                    self.currentlyPlayingLabel.text = "No song is currently playing"
                    self.playingListLabel.text = ""
                }
                else {
                    print(queuedSongNamesArray)
                    self.currentlyPlayingLabel.text = "Currently playing: \(queuedSongNamesArray[0])"
                    if (queuedSongNamesArray.count > 1) {
                        for i in 1..<queuedSongNamesArray.count {
                            self.playingListLabel.text?.append("\(i). \(queuedSongNamesArray[i])\n")
                            print("\(queuedSongNamesArray[i])")
                        }
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
