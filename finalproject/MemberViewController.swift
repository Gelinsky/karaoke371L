//
//  MemberViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/8/21.
//

import UIKit
import Firebase
import CoreData
import FirebaseFirestore

class MemberViewController: UIViewController {

    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    @IBOutlet weak var membersLabel: UILabel!
    @IBOutlet weak var roomCodeLabel: UILabel!
    
    var roomCode: String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roomCode = globalRoomCode
        // is scroll view needed for members?
        
      
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
            // Do any additional setup after loading the view.
        
        view.backgroundColor = backgroundMode
        
        if let value = roomCode {
            roomCodeLabel.text = "Room Code: \(value)"
        }
        
        let db = Firestore.firestore()
        let docRef = db.collection("rooms").document(roomCode)
        docRef.getDocument {(document, error) in
            if let document = document, document.exists {
                let memberArray = document["memberNames"] as? Array ?? []
                var members = ""
                for name in memberArray {
                    members += name as! String + "\n"
                }
                self.membersLabel.text = members
            }
            else {
                print("error in finding room")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
