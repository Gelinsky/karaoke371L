//
//  SetNameViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/6/21.
//

import UIKit
import Firebase
import CoreData
import FirebaseFirestore


class SetNameViewController: UIViewController {

    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    @IBOutlet weak var setNameTextField: UITextField!
    @IBOutlet weak var setNameLabel: UILabel!
    var roomCode: String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
        view.backgroundColor = backgroundMode
        
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController()?.gestureEnabled = false
        
        // name has already been made, allow user to change name
        if (name != "") {
//            let db = Firestore.firestore()
//            let docRef = db.collection("rooms").document(globalRoomCode)
//            docRef.getDocument {(document, error) in
//                if let document = document, document.exists {
//                    var memberArray = document["memberNames"] as? Array ?? [""]
//                    let queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
//                    let queuedSongURLsArray = document["queuedSongUrls"] as? Array ?? []
//                    print(memberArray)
//                    let index = memberArray.firstIndex(of: self.setNameTextField.text ?? "")
//                    if (index != nil) {
////                        memberArray.remove(at: index)
////                        print("remove name successful")
                        self.setNameLabel.text = "Change your name!"
//                    }
//                    else {
//                        print("error in finding name")
//                    }
//                }
//            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController()?.gestureEnabled = true
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        // error check name
        // store name in document
        // segue to next view controller
        if (setNameTextField.text?.count == 0) {
            let alert = UIAlertController(title: "Invalid name entered", message: "Name must be at least 1 character long", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
            self.present(alert, animated: true)
        }
        else {
            // first time putting name
//            if (name == "") {
                // check if the name already exists
                let db = Firestore.firestore()
                let docRef = db.collection("rooms").document(globalRoomCode)
                docRef.getDocument {(document, error) in
                    if let document = document, document.exists {
                        var memberArray = document["memberNames"] as? Array ?? [""]
                        let queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
                        let queuedSongURLsArray = document["queuedSongUrls"] as? Array ?? []
                        print(memberArray)
                        let index = memberArray.firstIndex(of: self.setNameTextField.text ?? "")
                        if index != nil {
                            // member with this name already exists
                            let alert = UIAlertController(title: "Invalid name entered", message: "A member with this name already exists, please choose a new name", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
                            self.present(alert, animated: true)
                        }
                        else {
                            // does not exist, add to member list
                            // name change is occurring, delete old name
                            var segueNeeded = true
                            if (name != "") {
                                segueNeeded = false
                                let index = memberArray.firstIndex(of: name ?? "")
                                print(name)
                                if (index != nil) {
                                    memberArray.remove(at: index!)
                                    print("remove name successful")
                                    self.setNameLabel.text = "Name change successful!"
                                }
                                else {
                                    print("error in finding name")
                                }
                            }
                            memberArray.append(self.setNameTextField.text ?? "")
                            let docData: [String: Any] = [
                                "memberNames": memberArray,
                                "queuedSongNames": queuedSongNamesArray,
                                "queuedSongUrls": queuedSongURLsArray
                            ]
                            let db = Firestore.firestore()
                            db.collection("rooms").document(globalRoomCode).setData(docData) { err in
                                if let err = err {
                                    print("error writing doc")
                                }
                                else {
                                    print("doc written successfully")
                                    name = self.setNameTextField.text
                                    print("saved name")
                                    print(name)
                                    self.setNameTextField.text = ""
                                    print(name)
                                    
                                    if segueNeeded {
                                        self.performSegue(withIdentifier: "NameSetSegueIdentifier", sender: nil)
                                    }
                                    else {
                                        let alert = UIAlertController(title: "Name changed successfully!", message: "Your name has changed", preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: "OK", style:.default, handler: nil))
                                        self.present(alert, animated: true)
                                    }
                                }
                            }
                        }
                    }
                    else {
                        print("error in set name vc, room code not valid")
                    }
            }
        }
    }
    
    // don't want to segue immediately, only on successful sign in/up
    override func shouldPerformSegue(withIdentifier identifier:String, sender: Any?) -> Bool {
        return false
    }
    
    // code to enable tapping on the background to remove software keyboard
        
        func textFieldShouldReturn(textField:UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
