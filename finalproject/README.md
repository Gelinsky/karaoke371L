//
//  README.md
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/13/21.
//

Team members: Malithy Wimalasooriya, Karl Gelinsky
Name of project: KaraOkay!
Dependencies: Xcode 12.3, Swift 5

Special Instructions:
- Use an iPhone 12 Pro Max simulator to join a room
- Use a device to create a room (as the host) in order to authenticate with Spotify (a Premium account is needed)
- Before running the app, run pod install inside of the project folder "karaoke371L")
- You can use swipe gestures to view the side menu on the left
- To test the app as the host and as a member of the room (but not the host), you need to run it on an iPad/iPhone
and a simulator at the same time

Feature                                         Description                                                     Who/Percentage worked on
Creating/Joining Room                User can join a room or host a room              Malithy (100%)
Spotify Authentication                  Spotify account is linked for host                   Karl (100%)
UI                                                 Colors, buttons, icons, navigation                   Malithy (80%), Karl (20%)
Queue                                           Updating queue screen                                  Karl (70%), Malithy (30%)                 
Storing Song Info in Firebase       Queued songs, song urls                               Karl (90%), Malithy (10%)
Settings                                        Dark Mode, Leave Room                                Malithy (100%)
Side Menu                                   Accessibility of other screens                          Malithy (100%)
Adding Songs                              Searching for songs in spotify and queuing    Karl (100%)
Lyrics Screen                               Searching for lyrics on Genius, web view        Karl (80%), Malithy (20%)
Storing Name Info in Firebase     Storing, deleting, changing name                    Malithy (100%) 

