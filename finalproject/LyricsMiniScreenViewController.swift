//
//  LyricsMiniScreenViewController.swift
//  finalproject
//
//  Created by Malithy Wimalasooriya on 8/11/21.
//

import UIKit
import WebKit
import FirebaseFirestore

class LyricsMiniScreenViewController: UIViewController {

    @IBOutlet var containerView : UIView? = nil
    var webView: WKWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = backgroundMode
        
        let db = Firestore.firestore()
        let docRef = db.collection("rooms").document(globalRoomCode)
        docRef.getDocument {(document, error) in
            if let document = document, document.exists {
                let queuedSongNamesArray = document["queuedSongNames"] as? Array ?? []
                if queuedSongNamesArray.isEmpty {
                    // TODO
//                    self.currentlyPlayingLabel.text = "No song is currently playing"
//                    self.playingListLabel.text = ""
                    let url = NSURL(string:"https://genius.com/")
                    let req = NSURLRequest(url:url as! URL)
                    self.webView!.load(req as URLRequest)
                }
                else {
                    // doesn't work when there are parentheses eg. (remix)
                    var songUrl: String = ""
                    let artistAndSong = (queuedSongNamesArray[0] as! String).components(separatedBy: " - ")
                    for word in artistAndSong[0].components(separatedBy: " "){
                        songUrl += word + "-"
                    }
                    for word in artistAndSong[1].components(separatedBy: " "){
                        songUrl += word + "-"
                    }
                    songUrl += "lyrics"
                    print(songUrl)
                    let url = NSURL(string:"https://genius.com/" + songUrl)
                    let req = NSURLRequest(url:url as! URL)
                    self.webView!.load(req as URLRequest)
                }
            }
        }
    }
    
    override func loadView(){
        super.loadView()
        self.webView = WKWebView()
        self.view = self.webView
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
